<!DOCTYPE html>
<html>
<head>
	<title>Ngọc Nguyễn</title>
</head>
<body>
  <div class="well" style="margin:auto; padding:auto; width:80%;"> 
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  	<div class="container-fluid">
  <center><h1>QUẢN LÝ NHÂN VIÊN</h1></center>
  <br>
    <div style="padding: 20px">
    <a href="them.php"><button class="btn btn-primary" style="width: 80px; " ><span class="glyphicon glyphicon-plus"></span>ADD</span></button></a>
    </div>

  <?php
  include("connect.php");

  $data = '<table class="table">
        <tr>
          <th>ID</th>
          <th>Full_name</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Address</th>
          <th>Function</th>
        </tr>';
    $query = "SELECT * From thongtinnhanvien";

    if (!$result = mysqli_query($con, $query)) 
    {
        exit(mysqli_error($con));
    }
    if(mysqli_num_rows($result) > 0)
    {
      while($row = mysqli_fetch_assoc($result))
      {
        $data .= '<tr>
          <td>'.$row['ID'].'</td>
          <td>'.$row['Full_name'].'</td>
          <td>'.$row['Phone'].'</td>
          <td>'.$row['Email'].'</td>
          <td>'.$row['Address'].'</td>
          <td>
              <a href="update.php?ID='.$row['ID'].'&Full_name ='.$row['Full_name'].' &Phone='.$row['Phone'].' &Email = '.$row['Email'].' &Address='.$row['Address'].'" ><button class="btn btn-success" style="width: 100px"><span class="glyphicon glyphicon-edit"></span>UPDATE</button>
              
              <a href="delete.php?ID='.$row['ID'].'" onclick="return checkdelete()"><button class="btn btn-danger" style="width:100px"><span class="glyphicon glyphicon-trash"></span>DELETE</button></a>
          </td>
        </tr>';
            
      }
    }
    else
    {
        $data .= '<tr><td colspan="6">Records not found!</td></tr>';
    }
 
    $data .= '</table>';
 
    echo $data;

?>  
</div>
<script >
  function checkdelete(){
    return confirm('Ban chac chan muon xoa???');
  }
</script>
</body>
</html>