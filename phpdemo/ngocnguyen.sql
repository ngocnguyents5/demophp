-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2019 at 03:36 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ngocnguyen`
--

-- --------------------------------------------------------

--
-- Table structure for table `thongtinnhanvien`
--

CREATE TABLE IF NOT EXISTS `thongtinnhanvien` (
`ID` int(15) NOT NULL,
  `Full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `thongtinnhanvien`
--

INSERT INTO `thongtinnhanvien` (`ID`, `Full_name`, `Phone`, `Email`, `Address`) VALUES
(1, '<br /><b>Notice</b>:  Undefined index: Full_name in <b>C:xampphtdocsupdate.php</b> on line <b>30</b><br />', '0981792340 ', '<br /><b>Notice</b>:  Undefined index: Email in <b>C:xampphtdocsupdate.php</b> on line <b>46</b><br />', '195 Tran Cung'),
(2, 'Trinh Thi Le', '0971252354', 'letrinh.ltt@gmail.com', 'Ngo 2 Pham Van Dong'),
(3, 'Nguyen Duy Cuong', '0963452378', 'cuongnguyen@gmail.com', 'Pham Van Dong'),
(4, 'Tran Thi Ha', '0972357120', 'hatran@gmail.com', 'Ngo 2 Pham Van Dong'),
(5, 'Nguyễn Trọng Tú', '0868234513', 'tunguyen@gmail.com', 'Trần Cung'),
(6, 'Duy Bình', '0868901702', 'duybinh@gmail.com', 'Hoài Đức');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `thongtinnhanvien`
--
ALTER TABLE `thongtinnhanvien`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `thongtinnhanvien`
--
ALTER TABLE `thongtinnhanvien`
MODIFY `ID` int(15) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
